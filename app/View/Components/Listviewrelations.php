<?php

namespace App\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Listviewrelations extends Component
{
    public $registros;
    public string $modelo;
    public array $campos;

    /**
     * Create a new component instance.
     */
    public function __construct($registros, string $modelo, array $campos = [])
    {
        $this->registros = $registros;
        $this->modelo = $modelo;

        if (empty($campos)) {
            // $registros[0]->getAttributes() un array con los valores del modelo
            // leo los campos de mi tabla desde un registro
            $campos = array_keys($registros[0]->getAttributes());
        }
        // voy a crear un array que le voy a pasar a la vista y asi solamente
        // la vista tiene que mostrar con una logica
        $this->campos = [];
        // voy a recorrer el argumento de los campos
        foreach ($campos as $value) {
            // compruebo si el texto contiene un punto
            // si contiene punto es una relacion
            // si no contiene punto es un campo
            if (str_contains($value, '.')) {
                // hasta el punto esta el nombre de la relacion
                $relacion = substr($value, 0, strpos($value, '.'));
                // desde el punto esta el nombre del campo
                $campo = substr($value, strpos($value, '.') + 1);
                // añado a la propiedad campos (que es un array) un nuevo elemento
                // [
                //     'valor' =>  funcion closure (variables que pasare desde la vista) use (variables que paso ahora)
                // ]
                $this->campos[] = ['valor' => function ($modelo) use ($relacion, $campo) {
                    return $modelo->$relacion->$campo;
                }, 'label' => $registros[0]->$relacion->getAttributeLabel($campo)];
            } else {
                // aqui entro cuando es un campo normal

                // añado a la propiedad campos (que es un array) un nuevo elemento
                // [
                //     'valor' =>  funcion closure (variables que pasare desde la vista) use (variables que paso ahora)
                // ]
                $this->campos[] = ['label' => $registros[0]->getAttributeLabel($value), 'valor' => function ($modelo) use ($value) {
                    return $modelo->$value;
                }];
            }
        }
    }
    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.listviewrelations');
    }
}
