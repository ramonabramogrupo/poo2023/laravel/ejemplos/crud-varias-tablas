<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Schema;

class Producto extends Model
{
    use HasFactory;

    // colocar el nombre de la tabla
    protected $table = 'productos';

    // colocar los atributos de la tabla que admitan asignacion masiva
    protected $fillable = [
        'nombre',
        'precio'
    ];

    public static $labels = [
        'id' => 'Id del producto',
        'nombre' => 'Nombre del producto',
        'precio' => 'Precio del producto',
    ];

    public function productotiendas(): HasMany
    {
        return $this->hasMany(Productotienda::class);
    }

    public function getAttributeLabel($attribute)
    {
        return self::$labels[$attribute] ?? $attribute;
    }
    public function getFields()
    {
        return Schema::getColumnListing($this->table);
    }
}
