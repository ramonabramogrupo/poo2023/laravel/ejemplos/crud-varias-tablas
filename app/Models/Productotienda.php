<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Schema;

class Productotienda extends Model
{
    use HasFactory;

    // nombre de la tabla
    protected $table = 'productotiendas';

    // campos asignacion masiva
    protected $fillable = [
        'cantidad',
        'tienda_id',
        'producto_id'
    ];

    public static $labels = [
        'id' => 'Id de la compra',
        'tienda_id' => 'Id de la tienda',
        'producto_id' => 'Id del producto',
        'nombretienda' => 'Nombre de la tienda ',
        'nombreproducto' => 'Nombre del producto',
    ];

    public function tienda(): BelongsTo
    {
        return $this->belongsTo(Tienda::class);
    }

    public function producto(): BelongsTo
    {
        return $this->belongsTo(Producto::class);
    }

    /**
     * Creo una nueva propiedad en el modelo
     * para mostrar el nombre de la tienda
     */

    public function getNombretiendaAttribute()
    {
        return $this->tienda->nombre;
    }

    public function getNombreproductoAttribute()
    {
        return $this->producto->nombre;
    }
    public function getAttributeLabel($attribute)
    {
        return self::$labels[$attribute] ?? $attribute;
    }

    public function getFields()
    {
        return Schema::getColumnListing($this->table);
    }
}
