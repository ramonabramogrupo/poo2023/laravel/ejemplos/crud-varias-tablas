<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Schema;

class Tienda extends Model
{
    use HasFactory;

    // nombre de la tabla
    protected $table = 'tiendas';

    // campos asignacion masiva
    protected $fillable = [
        'nombre',
        'ubicacion'
    ];

    public static $labels = [
        'id' => 'Id de la tienda',
        'nombre' => 'Nombre de la tienda',
        'ubicacion' => 'Ubicacion de la tienda',
    ];

    public function productotiendas(): HasMany
    {
        return $this->hasMany(Productotienda::class);
    }

    public function getAttributeLabel($attribute)
    {
        return self::$labels[$attribute] ?? $attribute;
    }
    public function getFields()
    {
        return Schema::getColumnListing($this->table);
    }
}
