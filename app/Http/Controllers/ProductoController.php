<?php

namespace App\Http\Controllers;

use App\Models\Producto;
use Illuminate\Http\Request;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        // listo los productos
        // $productos = Producto::all();
        // listo los productos paginados
        $productos = Producto::paginate(9);


        // $productos es un array(coleccion) de modelos
        // $productos[0] es un modelo (clase)
        // $productos[0]->nombre es un string con el nombre del producto
        // $productos[0]->getAttributes() es un array con los atributos de la tabla y valores
        // $productos[0]->toArray() es un array con los atributos de la tabla y valores mostrando campos calculados que no estan en la tabla
        // $producto->attributesToArray() es un array con los atributos de la tabla y valores


        // $productos->load('productotiendas') es para ejecutar las relaciones en todos los modelos
        // $productos[0]->getRelations() es para devolverte un array de las relaciones como modelos
        // $productos[0]->relationsToArray() es para devolverte un array de las relaciones pero como array
        // $productos[0]->getAttributes() es para devolverte un array con los atributos de la tabla y valores
        // $productos[0]->toArray() es para devolverte un array con los atributos de la tabla y valores mas las relaciones

        // los mando a la vista
        return view(
            'producto.index',
            compact('productos')
        );
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('producto.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //validaciones
        $request->validate([
            'nombre' => 'required',
            'precio' => 'required|numeric|max:1000',
        ], [
            'required' => 'El campo :attribute es obligatorio',
            'precio.numeric' => 'El campo precio del producto debe ser numerico',
            'precio.max' => 'El campo precio del producto no debe ser mayor a 1000',
        ], [
            'nombre' => 'Nombre del producto',
            'precio' => 'Precio del producto',
        ]);

        $producto = Producto::create($request->all());
        // sin asignacion masiva

        // $producto = new Producto();
        // $producto->nombre= $request->nombre;
        // $producto->precio= $request->precio;
        // $producto->save();

        return redirect()
            ->route('producto.show', $producto)
            ->with('mensaje', 'Producto creado');
    }

    /**
     * Display the specified resource.
     */
    public function show(Producto $producto)
    {
        return view(
            'producto.show',
            compact('producto')
        );
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Producto $producto)
    {
        return view(
            'producto.edit',
            compact('producto')
        );
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Producto $producto)
    {
        //validaciones
        $request->validate([
            'nombre' => 'required',
            'precio' => 'required|numeric|max:1000',
        ], [
            'required' => 'El campo :attribute es obligatorio',
            'precio.numeric' => 'El campo precio del producto debe ser numerico',
            'precio.max' => 'El campo precio del producto no debe ser mayor a 1000',
        ], [
            'nombre' => 'Nombre del producto',
            'precio' => 'Precio del producto',
        ]);

        // actualizar el registro
        $producto->update($request->all());

        return redirect()
            ->route('producto.show', $producto)
            ->with('mensaje', 'Producto actualizado');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Producto $producto)
    {
        // eliminamos el registro
        $producto->delete();

        // redireccionamos
        return redirect()
            ->route('producto.index')
            ->with('mensaje', 'Producto eliminado');
    }
}
