<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TiendaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'nombre' => 'required|max:100|min:3',
            'ubicacion' => 'required',
        ];
    }

    /**
     * personalizo los mensajes de error
     */
    public function messages()
    {
        return [
            'required' => 'El campo :attribute es necesario rellenarlo.',
            'nombre.max' => 'El campo :attribute no puede tener mas de :max caracteres.',
            'min' => 'El campo :attribute no puede tener menos de :min caracteres.',
        ];
    }

    /**
     * personalizo los nombres de los campos en los mensajes de error
     */
    public function attributes()
    {
        return [
            'nombre' => 'nombre de la tienda',
            'ubicacion' => 'ubicacion de la tienda',
        ];
    }
}
