<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductoController;
use App\Http\Controllers\ProductotiendaController;
use App\Http\Controllers\TiendaController;
use Illuminate\Support\Facades\Route;

// con la uri '/' cargar la accion index del controlador home

// Route::get(
//     '/',  // URI
//     [
//         HomeController::class, // Controlador
//         'index', // accion
//     ]
// )->name('home');

Route::controller(HomeController::class)->group(function () {
    Route::get(
        '/',  // uri
        'index' // accion
    )->name('home');
});

Route::resource('producto', ProductoController::class);
Route::resource('tienda', TiendaController::class);
Route::resource('productotienda', ProductotiendaController::class);

// Route::controller(ProductoController::class)->group(function () {
//     Route::get('/producto', 'index')->name('producto.index');
//     Route::get('/producto/create', 'create')->name('producto.create');
//     Route::post('/producto', 'store')->name('producto.store');
//     Route::get('/producto/edit/{id}', 'edit')->name('producto.edit');
//     Route::get('/producto/{id}', 'show')->name('producto.show');
//     Route::put('/producto/{producto}', 'update')->name('producto.update');
//     Route::delete('/producto/{producto}', 'destroy')->name('producto.destroy');
// });
