<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('productotiendas', function (Blueprint $table) {
            $table->id();
            $table->integer('cantidad');
            // crear claves ajenas
            // productotiendas_id
            $table->unsignedBigInteger('producto_id');
            $table
                ->foreign('producto_id') // nombre del campo que es clave ajena
                ->references('id') //nombre del campo en la tabla principal
                ->on('productos') // nombre de la tabla
                ->onDelete('cascade') // propiedades de la clave ajena
                ->onUpdate('cascade');

            // tiendas_id
            $table->unsignedBigInteger('tienda_id');
            $table
                ->foreign('tienda_id')
                ->references('id')
                ->on('tiendas')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            // indexado sin duplicados 
            $table->unique(['producto_id', 'tienda_id']);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('productotiendas');
    }
};
