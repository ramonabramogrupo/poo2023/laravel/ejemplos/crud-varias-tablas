<?php

namespace Database\Seeders;

use App\Models\Producto;
use Database\Factories\ProductoFactory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Producto::factory(50)->create();
    }
}
