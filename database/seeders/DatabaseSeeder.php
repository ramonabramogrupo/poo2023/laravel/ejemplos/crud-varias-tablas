<?php

namespace Database\Seeders;

use App\Models\Producto;
use App\Models\Tienda;
use App\Models\User;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // 1 opcion
        // Producto::factory(30)->create();
        // Tienda::factory(30)->create();
        // Productotienda::factory(30)->create(); // da error


        // 2 opcion
        $this->call([
            ProductoSeeder::class,
            TiendaSeeder::class,
            ProductotiendaSeeder::class
        ]);
    }
}
