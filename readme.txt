laravel new aplicacion6
cd aplicacion6
npm i

npm i sass --save-dev
npm i bootstrap --save-dev

cambiar el nombre del archivo css en resources => scss

dentro de la configuracion de vite ==> vite.config.js cambiar el nombre al css

en el fichero scss introducir bootstrap

en el fichero js introducir bootstrap

colocar en el .env idioma castellano

crear un controlador denominado Home para las acciones del sitio principal
php artisan make:controller HomeController

creo un metodo index que llame a la vista home.index

creo la vista index dentro de views en una carpeta llamada home

ir al gestor de rutas (web) y colocar la ruta del URI '/' ==> accion index del controlador home ==> llama a la vista home.index

crear un modelo, un controlador de recursos, un factory, un seeder para la tabla Productos

php artisan make:model Producto -mfscr

ir al fichero de migraciones y crear los campos de la tabla Productos

ejecuto las migraciones
php artisan migrate

meter productos utilizando seeder y factories

ejecuto los seeder
php artisan db:seed 

Creo una carpeta llamada layouts en views
	- coloco el layout llamado main
Creo una carpeta llamada _comun en views
	- coloco el menu principal (menu)
	- coloco el pie (pie)


Ahora realizo el CRUD de productos

-- LISTAR PRODUCTOS
-1 accion del controlador index
-2 vista index (creo en una carpeta llamada producto)



npm run dev












