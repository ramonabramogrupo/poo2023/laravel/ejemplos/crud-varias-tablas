@extends('layouts.main')

@section('titulo', 'Mostrar')

@section('cabecera')
    <section class="pt-5 container">
        <div class="row py-lg-5">
            <h1 class="display-4 fw-bold lh-1 mb-3">Ver Compra</h1>
            <p class="col-lg-10 fs-4">
                Estos son los datos de la compra
            </p>
        </div>
    </section>
@endsection

@section('contenido')
    @if (session('mensaje'))
        <div class="row m-2">
            <div class="alert alert-info">
                {{ session('mensaje') }}
            </div>
        </div>
    @endif
    <div class="row mt-3">
        <div class="col">
            <div class="card shadow-xl">
                <div class="card-body">
                    <h5 class="card-title">
                        {{ $productotienda->id }}
                    </h5>
                    <p class="card-text">
                        producto_id: {{ $productotienda->producto_id }}
                    </p>
                    <p class="card-text">
                        Nombre producto: {{ $productotienda->producto->nombre }}
                    </p>

                    <p class="card-text">
                        Tienda_id: {{ $productotienda->tienda_id }}
                    </p>
                    <p class="card-text">
                        Nombre de la tienda: {{ $productotienda->tienda->nombre }}
                    </p>
                    <p class="card-text">
                        Cantidad: {{ $productotienda->cantidad }}
                    </p>


                    <div class="card-footer">
                        <div class="vertical-align-bottom d-flex justify-content-between align-items-center">
                            <a href="{{ route('productotienda.edit', $productotienda) }}" class="btn btn-primary">Editar</a>
                            <form action="{{ route('productotienda.destroy', $productotienda) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger">Eliminar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
