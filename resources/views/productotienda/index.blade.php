@extends('layouts.main')

@section('titulo', 'Listado de Compras')

@section('cabecera')
    <x-cabecera subtitulo="Listado de Compras">
        <i class="fa-solid fa-cart-shopping"></i> Listado de Compras
    </x-cabecera>

@endsection

@section('contenido')
    @if (session('mensaje'))
        <div class="row m-3">
            <div class="alert alert-info">
                {{ session('mensaje') }}
            </div>
        </div>
    @endif

    <x-listviewrelations :registros="$productotienda" modelo="productotienda" :campos="[
        'cantidad',
        'tienda_id',
        'producto_id',
        'nombretienda',
        'nombreproducto',
        'tienda.nombre',
        'tienda.ubicacion',
        'producto.nombre',
        // [
        //     'label' => 'Nombre de la tienda',
        //     'campo' => 'nombre',
        //     'relacion' => 'tienda',
        // ],
        // [
        //     'label' => 'Nombre del producto',
        //     'valor' => function ($modelo) {
        //         return $modelo->tienda->nombre;
        //     },
        // ],
    ]" />

@endsection
