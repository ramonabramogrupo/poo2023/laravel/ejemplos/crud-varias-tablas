@extends('layouts.main')

@section('titulo', 'Listado de Productos')

@section('cabecera')
    <x-cabecera subtitulo="Listado de Productos">
        <i class="fa-solid fa-cart-shopping"></i> Listado de Productos
    </x-cabecera>

@endsection

@section('contenido')
    @if (session('mensaje'))
        <div class="row m-3">
            <div class="alert alert-info">
                {{ session('mensaje') }}
            </div>
        </div>
    @endif

    <x-listview :registros="$productos" modelo="producto" :campos="['id', 'precio', 'nombre']" />

@endsection
