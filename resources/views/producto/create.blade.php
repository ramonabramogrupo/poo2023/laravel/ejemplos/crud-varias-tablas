@extends('layouts.main')

@section('titulo', 'Crear Producto')

@section('cabecera')
    <section class="pt-5 container">
        <div class="row py-lg-5">
            <h1 class="display-4 fw-bold lh-1 mb-3">Insertar Producto</h1>
            <p class="col-lg-10 fs-4">
                Insertando un nuevo producto
            </p>
        </div>
    </section>
@endsection

@section('contenido')
    <x-formularios.formulario :errors="$errors" method="POST" route="{{ route('producto.store') }}">
        <x-formularios.input label="Nombre" name="nombre" :value="old('nombre')" />
        <x-formularios.input label="Precio" name="precio" :value="old('precio')" />
    </x-formularios.formulario>
@endsection
