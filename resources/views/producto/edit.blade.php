@extends('layouts.main')

@section('titulo', 'Editar Producto')

@section('cabecera')
    <section class="pt-5 container">
        <div class="row py-lg-5">
            <h1 class="display-4 fw-bold lh-1 mb-3">Editar Producto</h1>
            <p class="col-lg-10 fs-4">
                Editando producto
            </p>
        </div>
    </section>
@endsection

@section('contenido')
    <x-formularios.formulario :modelo="$producto" :errors="$errors" method="PUT"
        route="{{ route('producto.update', $producto) }}">
        <x-formularios.input label="Nombre" name="nombre" :value="old('nombre', $producto->nombre)" />
        <x-formularios.input label="Precio" name="precio" :value="old('precio', $producto->precio)" />
    </x-formularios.formulario>
@endsection
