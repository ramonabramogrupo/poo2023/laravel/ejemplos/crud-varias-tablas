<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="{{ route('home') }}">
            Gestion de productos y tiendas
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link{{ request()->routeIs('home') ? ' active' : '' }}" aria-current="page"
                        href="{{ route('home') }}">Inicio</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request()->routeIs('producto.index') ? ' active' : '' }}"
                        href="{{ route('producto.index') }}">Productos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request()->routeIs('producto.create') ? ' active' : '' }}"
                        href="{{ route('producto.create') }}">Insertar Producto</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request()->routeIs('tienda.index') ? ' active' : '' }}"
                        href="{{ route('tienda.index') }}">Tiendas</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request()->routeIs('tienda.create') ? ' active' : '' }}"
                        href="{{ route('tienda.create') }}">Insertar Tienda</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request()->routeIs('productotienda.index') ? ' active' : '' }}"
                        href="{{ route('productotienda.index') }}">Compras</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
