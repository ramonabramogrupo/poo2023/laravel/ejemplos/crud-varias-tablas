@extends('layouts.main')

@section('titulo', 'Crear Tienda')

@section('cabecera')
    <section class="pt-5 container">
        <div class="row py-lg-5">
            <h1 class="display-4 fw-bold lh-1 mb-3">Insertar Tienda</h1>
            <p class="col-lg-10 fs-4">
                Insertando una nueva tienda
            </p>
        </div>
    </section>
@endsection

@section('contenido')
    <x-formularios.formulario :errors="$errors" method="POST" route="{{ route('tienda.store') }}">
        <x-formularios.input label="Nombre" name="nombre" :value="old('nombre')" />
        <x-formularios.input label="Ubicacion" name="ubicacion" :value="old('ubicacion')" />
    </x-formularios.formulario>
@endsection
