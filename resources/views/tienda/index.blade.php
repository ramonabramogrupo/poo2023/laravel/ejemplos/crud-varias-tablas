@extends('layouts.main')

@section('titulo', 'Listado de Tiendas')

@section('cabecera')
    <x-cabecera subtitulo="Listado de Tiendas">
        <i class="fa-solid fa-cart-shopping"></i> Listado de Tiendas
    </x-cabecera>
@endsection

@section('contenido')
    @if (session('mensaje'))
        <div class="row m-3">
            <div class="alert alert-info">
                {{ session('mensaje') }}
            </div>
        </div>
    @endif
    <x-listado :registros="$tiendas" modelo="tienda" :campos="['id', 'nombre', 'ubicacion']" />
@endsection
