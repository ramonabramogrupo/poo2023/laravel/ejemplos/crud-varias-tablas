@extends('layouts.main')

@section('titulo', 'Editar Tienda')

@section('cabecera')
    <section class="pt-5 container">
        <div class="row py-lg-5">
            <h1 class="display-4 fw-bold lh-1 mb-3">Editar Tienda</h1>
            <p class="col-lg-10 fs-4">
                Editando tienda
            </p>
        </div>
    </section>
@endsection

@section('contenido')
    <x-formularios.formulario :modelo="$tienda" :errors="$errors" method="PUT"
        route="{{ route('tienda.update', $tienda) }}">
        <x-formularios.input label="Nombre" name="nombre" :value="old('nombre', $tienda->nombre)" />
        <x-formularios.input label="Ubicacion" name="ubicacion" :value="old('precio', $tienda->ubicacion)" />
    </x-formularios.formulario>
@endsection
