<x-formularios.errors :errors="$errors" />
<div class="row mt-3">
    <div class="col-lg-10 mt-2 mx-auto">
        <form action="{{ $route }}" method="post" class="p-4 p-md-5 border rounded-3 bg-light">
            @csrf
            @method($method)

            {{ $slot }}

            <button type="submit" class="btn btn-primary">{{ $boton }}</button>
            <button type="reset" class="btn btn-primary">Limpiar</button>
    </div>
</div>
