# Objetivos

Componente anonimo que me permite mostrar en formato de tarjeta todos los registros de una consulta

# Uso

Para utilizarlo simplemente llamo al componente desde la vista y le mando el array de los modelos

# Parametros

- $registros : array de modelos
- $modelo : nombre del modelo que estamos utilizando
- $campos : campos a mostrar de la tabla. Si no le pasamos nada muestra todos los campos tanto los de la tabla como los personalizados en el modelo.


# Ejemplo

~~~php
    <x-listview :registros="$productos" modelo="producto" :campos="['id', 'nombre', 'precio']" />
~~~

Hay que tener en cuenta que la variable productos sera una consulta realizada en el controlador

Sin paginar
~~~php
$productos = Producto::all();
~~~
Listo los productos paginados

~~~php
$productos = Producto::paginate(9);
~~~