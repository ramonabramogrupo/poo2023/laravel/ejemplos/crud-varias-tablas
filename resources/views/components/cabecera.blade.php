@props([
    'subtitulo' => '',
])

<section class="pt-5 text-center container">
    <div class="row py-lg-5">
        <div class="col-lg-6 col-md-8 mx-auto">
            <h1 class="fw-light">{{ $slot }}</h1>
            <p class="lead text-muted">{{ $subtitulo }}</p>
        </div>
    </div>
</section>
