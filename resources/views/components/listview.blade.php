@props(['registros', 'modelo', 'campos' => []])

<div class="row mt-1 row-cols-1 row-cols-lg-3 row-cols-md-2 g-4">
    @foreach ($registros as $registro)
        <div class="col">
            <div class="card shadow-sm">
                <div class="card-body">
                    {{-- si campos no esta vacio solo muestro los campos pasados --}}
                    @if (!empty($campos))
                        @foreach ($campos as $value)
                            <p class="card-text">
                                {{ $registro->getAttributeLabel($value) }}: {{ $registro->$value }}
                            </p>
                        @endforeach
                        {{-- si campos esta vacio muestro todos los campos --}}
                    @else
                        @foreach ($registro->toArray() as $label => $value)
                            <p class="card-text">
                                {{ $registro->getAttributeLabel($label) }}: {{ $value }}
                            </p>
                        @endforeach
                    @endif

                </div>
                <div class="card-footer">
                    <div class="vertical-align-bottom d-flex justify-content-between align-items-center">
                        <div class="btn-group">
                            <a href="{{ route($modelo . '.show', $registro->id) }}"
                                class="btn btn-outline-primary">Ver</a>
                            <a href="{{ route($modelo . '.edit', $registro->id) }}"
                                class="btn btn-outline-primary">Editar</a>
                        </div>
                        <form action="{{ route($modelo . '.destroy', $registro) }}" method="post">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-outline-danger">Eliminar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>
<div class="row mt-3">
    {{-- controlar si tengo que realizar la paginacion --}}
    @if (method_exists($registros, 'links'))
        {{ $registros->links() }}
    @endif

</div>
